﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unistenje : MonoBehaviour {

    //public GameObject explosion;
    //public int scoreValue;
    public int zivotValue;
    private Done_GameController gameController;
    private Done_GameController gameController1;


    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
            gameController1 = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null || gameController1 == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        //gameController.AddScore(scoreValue);
        gameController1.AddZivot(zivotValue);
        //Instantiate(explosion, transform.position, transform.rotation);
        //Destroy(other.gameObject);
        //Destroy(gameObject);
    }
}
