﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dijamanti : MonoBehaviour {


    public GameObject explosion;
    //public int scoreValue;
    //public int zivotValue;
    public int dijamantValue;
    private Done_GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindGameObjectWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<Done_GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "dijamant")
        {
            return;
        }
        gameController.AddDijamant(dijamantValue);
        //gameController.AddScore(scoreValue);
        //gameController1.AddZivot(zivotValue);
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}

