﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Done_GameController : MonoBehaviour
{
	public GameObject[] hazards;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	
	public GUIText scoreText;
    public GUIText zivotText;
    public GUIText restartText;
    public GUIText dijamantTxet;
    public GUIText gameOverText;
    
    

    private bool gameOver;
	private bool restart;
	private int score;
    private int zivot;
    private int dijamant;
	
	void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
        zivotText.text = "";
        dijamantTxet.text = "";
        zivot = 100;
		score = 0;
        dijamant = 0;
		UpdateScore ();
        UpdateZivot();
        UpdateDijamant();
		StartCoroutine (SpawnWaves ());
	}
	
	void Update ()
	{
		if (restart)
		{
			if (Input.GetKeyDown (KeyCode.R))
			{
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
			}
		}
	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++)
			{
				GameObject hazard = hazards [Random.Range (0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
			
			if (gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}
	
	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}
	
	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;
	}

    public void AddDijamant(int newDijamantValue)
    {
        if (dijamant >=4)
        {
            //pobjednicki ekran
            SceneManager.LoadScene("Intro");
        }
        else
        dijamant += newDijamantValue;
        UpdateDijamant();
    }

    void UpdateDijamant()
    {
        dijamantTxet.text = "Dijamant: 5 / " + dijamant;
    }


    public void AddZivot(int newZivot) {
        if (zivot <= 1)
        {
            //GameOver();
            //ucitaj scenu gubitka
            //nova scena za izgubljeno

            SceneManager.LoadScene("Intro");

            //gameOverText.text = "Game Over!";
        }
        else
        zivot -= newZivot;
        UpdateZivot();
    }

    void UpdateZivot() {
        zivotText.text = "Zivot: " + zivot;
        Debug.Log(zivot);
    }

	
	public void GameOver ()
	{
		
		gameOver = true;
	}
}