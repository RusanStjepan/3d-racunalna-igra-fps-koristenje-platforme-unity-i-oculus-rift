﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class prati : MonoBehaviour {

    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    public float horizontalSpeed = 2.0F;
    public float verticalSpeed = 2.0F;

    void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        float h = horizontalSpeed * Input.GetAxis("Mouse X");
        float v = verticalSpeed * Input.GetAxis("Mouse Y");
        transform.Rotate(v, h, 0);
    }

    
    
}
